### NPM Install

```
{
  "name": "my-project",
  "dependencies": {
    "app.js": "git+https://bitbucket.org/scott_morken/app.js.git"
  }
}
```

### Gulpfile addition

```
...
  .copy('./bower_components/smorken.js/dist', './public/js/limited')
  .copy('./bower_components/app.js/dist', './public/js/limited')
...
```
