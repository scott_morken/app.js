/**
 * Created by smorken on 4/21/15.
 */

(function(wHnd, $) {
    var af = {
        urls: {},
        init: function() {
            app.frontend.ajaxSetup();
        },
        ajaxSetup: function() {
            if (typeof $ !== 'undefined') {
                var $loading = $('.loading').hide();
                $(document).ajaxStart(function () {
                    $loading.show();
                    $loading.center();
                });
                $(document).ajaxStop(function () {
                    $loading.hide();
                });
                $.ajaxSetup({
                    headers: {
                        'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
        },
        ajaxFail: function(data) {
            if (typeof $ !== 'undefined') {
                try {
                    var errordata = $.parseJSON(data.responseText);
                    console.log(errordata);
                }
                catch (e) {
                    errordata = {message: "Fatal error! At this point, that is all I can tell you."};
                }
                $('#modal-title').html("Error");
                $('#modal-body p').html(errordata.message);
                $('#modal').modal('show');
            }
        },
        getUrl: function(name) {
            if (app.frontend.urls && app.frontend.urls[name]) {
                return app.frontend.urls[name];
            }
            return null;
        },
        escape: function(str) {
            var d = document.createElement('div');
            d.appendChild(document.createTextNode(str));
            return d.innerHTML;
        },
        unescape: function(escaped) {
            var d = document.createElement('div');
            d.innerHTML = escaped;
            var child = d.childNodes[0];
            return child ? child.nodeValue : '';
        }
    };
    smorken.Extend('app.frontend', af);
})(window, jQuery);

if (typeof jQuery !== 'undefined') {
    jQuery.fn.center = function() {
        this.css("position", "absolute");
        this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
        this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
    };

    (function ($) {
        $.hexc = function(colorval) {
            var parts = colorval.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+),?\s*\d*\)$/);
            if (parts) {
                delete(parts[0]);
                for (var i = 1; i <= 3; ++i) {
                    parts[i] = parseInt(parts[i]).toString(16);
                    if (parts[i].length == 1) parts[i] = '0' + parts[i];
                }
                return '#' + parts.join('');
            }
        }
    }
    )(jQuery);

    (function ($) {
        var decode = function (str) {
            return decodeURIComponent(str.replace(/\+/g, ' '));
        };
        $.parseParams = function (query) {
            if (!query) {
                query = window.location.search;
            }
            var params = {}, e;
            if (query) {
                // remove # from end of query
                if (query.indexOf('#') !== -1) {
                    query = query.substr(0, query.indexOf('#'));
                }

                // remove ? at the begining of the query
                if (query.indexOf('?') !== -1) {
                    query = query.substr(query.indexOf('?') + 1, query.length);
                }
                // empty parameters
                if (query == '') return {};
                var pairs = query.split('&');
                pairs.forEach(function(pair) {
                    pair = pair.split('=');
                    params[pair[0]] = decode(pair[1] || '');
                });
            }
            return params;
        };
    })(jQuery);

    (function($) {
        $.sm = $.sm || {};
        $.sm.escape = function(str) {
            return app.frontend.escape(str);
        };
        $.sm.unescape = function(str) {
            return app.frontend.unescape(str);
        };
    })(jQuery);
}
